var iApp = (function() {


	function init() {

		proposeApp();
		trackApp();
		scanLinks();

	}


	function is_iOS() {

		if ( navigator.userAgent.toLowerCase().match(/(iphone|ipod|ipad)/) ) {
		  return true;
		}
		else {
		  return false;
		}

	}


	function is_iMobile() {

		if ( navigator.userAgent.toLowerCase().match(/(iphone|ipod)/) ) {
		  return true;
		}
		else {
		  return false;
		}

	}


	function is_iTablet() {

		if ( navigator.userAgent.toLowerCase().match('ipad') ) {
		  return true;
		}
		else {
		  return false;
		}

	}


	function is_Safari() {

		if ( is_iOS() && !navigator.userAgent.match('CriOS') ) {
		  return true;
		}
		else {
		  return false;
		}

	}


	function is_Standalone() {

		if (("standalone" in window.navigator) && window.navigator.standalone) {
		  return true;
		}
		else {
		  return false;
		}

	}


	function proposeApp() {

		if ( is_iOS() && is_Safari() && !is_Standalone() && !is_iTablet() )
		{

			$('.iphone').fadeIn();
			$('.iphone').click(function() {
			  $(this).fadeOut();
			});

		}

	}


	function trackApp() {
	
		if ( is_iOS() && is_Safari() && is_Standalone() )
		{
			_gaq.push(['_trackPageview', '/webApp']);
		}
		else {
			_gaq.push(['_trackPageview']);
		}
	
	}


	function scanLinks() {

		if ( is_iOS() && is_Safari() && is_Standalone() )
		{

			$('.share').find('a').removeAttr('onclick');	// in webapp, remove popup on share buttons
			$('.aligncenter').parent().attr('href', '#');	// in webapp, remove link on post image

			var curnode, location=document.location, stop=/^(a|html)$/i;

			document.addEventListener('click', function(e) {

				var elem = e.target.src;

				if (elem && elem.match('share'))
				{
					return false;
				}
				else {

					curnode=e.target;
					while (!(stop).test(curnode.nodeName)) {
						curnode=curnode.parentNode;
					}

					if( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) {
						e.preventDefault();
						location.href = curnode.href;
					}

				}

			}, false);

		}

	}


	return { init : init };


})();
